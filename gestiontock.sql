CREATE TABLE products(
   id_product INT,
   name_product VARCHAR(50),
   description_product TEXT,
   default_price DOUBLE,
   PRIMARY KEY(id_product)
);

CREATE TABLE categories(
   id_category INT,
   name_category VARCHAR(50),
   PRIMARY KEY(id_category)
);

CREATE TABLE types(
   id_type INT,
   name_type VARCHAR(50),
   PRIMARY KEY(id_type)
);

CREATE TABLE variants(
   id_variant INT,
   name_variant VARCHAR(50),
   price_variant DOUBLE,
   price_reduct DOUBLE,
   PRIMARY KEY(id_variant)
);

CREATE TABLE images(
   id_image INT,
   name_image VARCHAR(50),
   link_image VARCHAR(255),
   PRIMARY KEY(id_image)
);

CREATE TABLE promotions(
   id_promotion INT,
   date_start DATE,
   date_end DATE,
   reduct_percent INT,
   PRIMARY KEY(id_promotion)
);

CREATE TABLE users(
   id_user INT,
   username VARCHAR(50) NOT NULL,
   password VARCHAR(255),
   role VARCHAR(50) NOT NULL,
   city VARCHAR(50),
   created_at DATETIME,
   PRIMARY KEY(id_user)
);

CREATE TABLE scores(
   id_score INT,
   score DOUBLE,
   PRIMARY KEY(id_score)
);

CREATE TABLE reviews(
   id_review INT,
   review VARCHAR(255),
   PRIMARY KEY(id_review)
);

CREATE TABLE products_categories(
   id_product INT,
   id_category INT,
   PRIMARY KEY(id_product, id_category),
   FOREIGN KEY(id_product) REFERENCES products(id_product),
   FOREIGN KEY(id_category) REFERENCES categories(id_category)
);

CREATE TABLE products_types(
   id_product INT,
   id_type INT,
   PRIMARY KEY(id_product, id_type),
   FOREIGN KEY(id_product) REFERENCES products(id_product),
   FOREIGN KEY(id_type) REFERENCES types(id_type)
);

CREATE TABLE types_variants(
   id_type INT,
   id_variant INT,
   PRIMARY KEY(id_type, id_variant),
   FOREIGN KEY(id_type) REFERENCES types(id_type),
   FOREIGN KEY(id_variant) REFERENCES variants(id_variant)
);

CREATE TABLE variants_images(
   id_variant INT,
   id_image INT,
   PRIMARY KEY(id_variant, id_image),
   FOREIGN KEY(id_variant) REFERENCES variants(id_variant),
   FOREIGN KEY(id_image) REFERENCES images(id_image)
);

CREATE TABLE variants_promotions(
   id_variant INT,
   id_promotion INT,
   PRIMARY KEY(id_variant, id_promotion),
   FOREIGN KEY(id_variant) REFERENCES variants(id_variant),
   FOREIGN KEY(id_promotion) REFERENCES promotions(id_promotion)
);

CREATE TABLE products_reviews_users(
   id_product INT,
   id_user INT,
   id_review INT,
   PRIMARY KEY(id_product, id_user, id_review),
   FOREIGN KEY(id_product) REFERENCES products(id_product),
   FOREIGN KEY(id_user) REFERENCES users(id_user),
   FOREIGN KEY(id_review) REFERENCES reviews(id_review)
);

CREATE TABLE products_scores_users(
   id_product INT,
   id_user INT,
   id_score INT,
   PRIMARY KEY(id_product, id_user, id_score),
   FOREIGN KEY(id_product) REFERENCES products(id_product),
   FOREIGN KEY(id_user) REFERENCES users(id_user),
   FOREIGN KEY(id_score) REFERENCES scores(id_score)
);
